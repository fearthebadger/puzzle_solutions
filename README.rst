.. _puzzle_solutions:

################
Puzzle Solutions
################

There are sites out there to do programming puzzles. So I started this repo to keep track of the ones
I have done.

Oiler
=====

I am purposefully misspelling the name of who these problems are named after (Hint: Replace "Oi" with "Eu").
This is so that the solutions don't show up in search results. But if you look hard enough for math 
programing puzzles you'll find what these tie to.